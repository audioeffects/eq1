# simpleeq

This project contains a simple eq plugin.

## Building (CMake)

```
git submodule update --init # To ensure JUCE is pulled and exists

cmake -B cmake-build 
cmake --build cmake-build
```

## Building (Projucer)

Open projucer and then build through the preferred method and system (Linux Makefiles or Mac)
